// SPDX-FileCopyrightText: 2023 Davids Paskevics <davids.paskevics@gmail.com>
//
// SPDX-License-Identifier: EUPL-1.2

use std::{borrow::Cow, collections::HashMap};

use crate::{common, CaptivePortal};
use ureq::Agent;

pub(crate) struct TheCloudHospitalityProvider;

impl TheCloudHospitalityProvider {
    pub fn new() -> TheCloudHospitalityProvider {
        TheCloudHospitalityProvider
    }
}

impl CaptivePortal for TheCloudHospitalityProvider {
    fn can_handle(&self, ssid: &str) -> bool {
        ["aohostels FREE Wifi"].contains(&ssid)
    }

    fn login(&self, http_client: &Agent) -> Result<(), anyhow::Error> {
        println!("Receiving cookies and query parameters from captive portal");
        let resp = common::follow_automatic_redirect(http_client)?;
        let url: url::Url = resp.get_url().try_into()?;

        // TODO: Is this necessary? Previous request already queried the URL
        /*
        println!("Starting login flow");
        http_client.get("resp.get_url()").call()?;
        */

        // It's easier to extract the MAC address from the URL than asking our OS
        let params = HashMap::<Cow<str>, Cow<str>>::from_iter(url.query_pairs());
        let mac = &*params["MAC"];

        println!("Accepting terms");
        http_client
            .post("https://hospitality.thecloud.eu/cgi-bin/splash")
            .send_form(&[
                ("acceptTerms", "on"),
                ("language", "EN"),
                ("MAC", &mac),
                ("mode_skip.x", "1"),
                ("mode_skip.y", "1"),
                ("mode_skip", "connect"),
            ])?;

        Ok(())
    }
}
