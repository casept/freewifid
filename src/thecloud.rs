// SPDX-FileCopyrightText: 2022 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: EUPL-1.2

use crate::{common, CaptivePortal};
use ureq::Agent;

pub(crate) struct TheCloudProvider;

impl TheCloudProvider {
    pub fn new() -> TheCloudProvider {
        TheCloudProvider
    }
}

impl CaptivePortal for TheCloudProvider {
    fn can_handle(&self, ssid: &str) -> bool {
        ["thecloud", "WiFi Darmstadt"].contains(&ssid)
    }

    fn login(&self, http_client: &Agent) -> Result<(), anyhow::Error> {
        // untested, source https://github.com/blocktrron/wifi-darmstadt-autologin/blob/master/login.sh

        println!("Receiving cookies from captive portal");
        common::follow_automatic_redirect(http_client)?;

        println!("Starting login flow");
        http_client
            .get("https://service.thecloud.eu/service-platform/macauthlogin/v1")
            .call()?;

        println!("Accepting terms");
        http_client
            .post("https://service.thecloud.eu/service-platform/macauthlogin/v1/registration")
            .send_form(&[("terms", "true")])?;

        Ok(())
    }
}
