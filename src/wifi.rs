// SPDX-FileCopyrightText: 2022 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: EUPL-1.2

use networkmanager::devices::{Device, Wireless};
use networkmanager::NetworkManager;

use dbus::blocking::Connection;

use std::{cmp::Ordering, collections::HashMap, process::Command};

pub trait WifiManagement {
    fn reachable_networks(&self) -> anyhow::Result<Vec<String>>;
    fn connect(&self, ssid: &str) -> anyhow::Result<()>;
}

pub struct NetworkManagerWifi<'a> {
    nm: NetworkManager<'a>,
}

impl<'a> NetworkManagerWifi<'a> {
    pub fn new(dbus_connection: &'a Connection) -> Result<NetworkManagerWifi<'a>, dbus::Error> {
        Ok(NetworkManagerWifi {
            nm: NetworkManager::new(dbus_connection),
        })
    }
}

impl<'a> WifiManagement for NetworkManagerWifi<'a> {
    fn reachable_networks(&self) -> anyhow::Result<Vec<String>> {
        let devices = self.nm.get_devices()?;

        for device in devices {
            if let Device::WiFi(wd) = device {
                println!("Found wifi device");
                wd.request_scan(HashMap::new())?;
                let mut access_points = wd.get_access_points()?;
                access_points.sort_by(|a, b| {
                    a.strength()
                        .ok()
                        .and_then(|sa| b.strength().ok().and_then(|sb| sa.partial_cmp(&sb)))
                        .unwrap_or(Ordering::Equal)
                });
                return Ok(access_points
                    .iter()
                    .filter_map(|ap| ap.ssid().ok())
                    .collect::<Vec<String>>());
            }
        }

        Ok(Vec::new())
    }

    fn connect(&self, ssid: &str) -> anyhow::Result<()> {
        // networkmanager crate currently can't do this
        Command::new("nmcli")
            .arg("d")
            .arg("wifi")
            .arg("c")
            .arg(ssid)
            .status()?;

        Ok(())
    }
}
