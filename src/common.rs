// SPDX-FileCopyrightText: 2022 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: EUPL-1.2

use ureq::*;

use scraper::{Html, Selector};

use anyhow::anyhow;

use crate::GENERIC_CHECK_URL;

pub fn follow_automatic_redirect(http_client: &Agent) -> Result<Response, anyhow::Error> {
    Ok(http_client.get(GENERIC_CHECK_URL).call()?)
}

pub fn scrape_redirect_url(html: &str) -> anyhow::Result<String> {
    let document = Html::parse_document(html);
    let redirect_selector = Selector::parse("meta[http-equiv=\"refresh\"]")
        .map_err(|_| anyhow!("Selector didn't match"))?;
    let element = document
        .select(&redirect_selector)
        .next()
        .ok_or(anyhow::anyhow!("Selector didn't match"))?;
    let url: &str = element
        .value()
        .attr("content")
        .ok_or(anyhow::anyhow!("Attribute missing"))?;
    let find_pattern = "url=";
    let url_offset = url
        .find(find_pattern)
        .ok_or(anyhow!("Expected to find pattern {find_pattern:?}"))?
        + find_pattern.len();
    let url = &url[url_offset..];
    Ok(url.to_string())
}
