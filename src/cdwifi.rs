// SPDX-FileCopyrightText: 2023 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: EUPL-1.2

use crate::CaptivePortal;

use ureq::Agent;

pub(crate) struct CDProvider;

impl CDProvider {
    pub fn new() -> CDProvider {
        CDProvider
    }
}

impl CaptivePortal for CDProvider {
    fn can_handle(&self, ssid: &str) -> bool {
        ssid.contains("CDWiFi")
    }

    fn login(&self, client: &Agent) -> Result<(), anyhow::Error> {
        let _ = client
            .get("http://cdwifi.cz/portal/api/vehicle/gateway/user/authenticate?category=internet&url=http%3A%2F%2Fcdwifi.cz%2Fportal%2Fapi%2Fvehicle%2Fgateway%2Fuser%2Fsuccess&onerror=http%3A%2F%2Fcdwifi.cz%2Fportal%2Fapi%2Fvehicle%2Fgateway%2Fuser%2Ferror")
            .call();

        Ok(())
    }
}
