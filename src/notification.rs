// SPDX-FileCopyrightText: 2022 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: EUPL-1.2

use notify_rust::Notification;

pub enum Answer {
    Accept,
    Reject,
    AcceptAutomatically,
}

pub fn ask_network_verification(ssid: &str) -> anyhow::Result<Answer> {
    let mut accepted = Answer::Reject;
    Notification::new()
        .summary(&format!(r#"An available open Wi-Fi network was found: "{ssid}", do you want to accept its terms and conditions automatically?"#))
        .action("accept", "Accept")
        .action("reject", "Ignore Network")
        .action("noninteractive", "Accept for all networks")
        .show()?
        .wait_for_action(|action| {
            use Answer::*;
            accepted = match action {
                "accept" => Accept,
                "reject" => Reject,
                "noninteractive" => AcceptAutomatically,
                _ => Reject,
            }
        });
    Ok(accepted)
}
