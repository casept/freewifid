# Freewifid

Freewifid runs in the background to check if you are near a free WiFi network. If it finds one, it automatically connects to it and logs you into the captive portal.

## Development

### Adding a new network of a known provider

Many networks use the same provider in the background, for example you can often find the same hotsplots captive portal.
For these networks, it's enough to just add their name to the `can_handle` function of their provider module. For hotsplots, thats `src/hotsplots.rs`.

### Adding a new provider

A provider is a struct that implements the CaptivePortal trait. It also needs to be added to the main function.

### Adding an exception for a network

Sometimes its useful to disable freewifid when a certain network is in reach, to avoid switching between a free wifi and another one.
This can be done by adding something like the following in the config file in `~/.config/freewifid/config.toml`.

```toml
exception_ssids = ["eduroam"]
```

### Non-interactive mode

By default, freewifid will ask before connecting to a new network, to give you a chance to actually read the conditions it will accept.
This behaviour can be turned of by adding
```toml
noninteractive = true
```
to the config file.
